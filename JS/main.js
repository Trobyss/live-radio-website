let menu = document.querySelector('.menu')
let ham = document.querySelector('.content ul')
menu.onclick = () =>{
   menu.classList.toggle('open')
   ham.classList.toggle('open-content')
}
AOS.init({
   offset: -100
})
let portfolio = document.querySelector('#portfolio')
document.querySelectorAll('.block')[0].addEventListener('click',function(){
   addUiClass(portfolio)
})

function addUiClass(element){
   if(element.classList.contains('ui-click'))return
   element.classList.add('ui-click')
   setTimeout( function(){element.classList.remove('ui-click')}, 1000)
}