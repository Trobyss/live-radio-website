# !/bin/bash
# Get servers list:
set — f
# Variables from GitLab server:
# Note: They can’t have spaces!!
string=$DEPLOY_SERVER
# Iterate servers for deploy and pull last commit
echo “Deploy project on server ${string}”
ssh ubuntu@${string} “cd /var/www/html/live-radio-website && git stash && git checkout $CI_BUILD_REF_NAME && git stash && git pull origin master && sudo yarn install && sudo npm run production”
